﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

using static Perceptrons.Constants;

namespace Perceptrons
{
    public class Perceptron
    {
        List<Tuple<int, List<double>>> DFTs {get; set;}
        List<double> WeightList = new List<double>();
        public List<Tuple<int,double>> errors = new List<Tuple<int, double>>();
        double Theta;
        double LearningRate;
        Random r = new Random();
        public int Wanted { get; set; }
        

        public Perceptron(int wanted, List<Tuple<int, List<double>>> tuples)
        {
            for (int i = 0; i < 35; i++)
            {
                WeightList.Add(r.NextDouble());
                Wanted = wanted;
            }
            DFTs = tuples;
            Theta = r.NextDouble();
            LearningRate = LEARNING_RATE;
        }

        /// <summary>
        /// Calculates the perceptron's ouptut.
        /// <returns> int : -1 if perceptron does not fire and 1 if it does </returns>
        /// </summary>
        public double Output(List<double> list)
        {
            double tempOut = 0;
            for (int i = 0; i < 35; i++)
            {
                tempOut += WeightList[i] * list[i];
            }
            return tempOut;
        }

        public void Train(List<Tuple<int, List<int>>> trainingSet)
        {
            Debug.WriteLine($"Starting learning process of Perceprton for recognition of {Wanted}...");

            for (int i = 0; i < NO_OF_ITERATIONS; i++)
            {
                int randomNumber = r.Next() % trainingSet.Count;

                int correctOutput = trainingSet[randomNumber].Item1 == Wanted ? 1 : -1;

                var accordingDFT = DFTs.Where(tup => tup.Item1 == trainingSet[randomNumber].Item1).First().Item2;

                double perceptronOutput = Output(accordingDFT);

                double err = correctOutput - perceptronOutput;
                errors.Add(new Tuple<int, double> (i, Math.Pow(perceptronOutput - correctOutput, 2)));

                for (int j = 0; j < WeightList.Count; j++)
                {
                    WeightList[j] += LearningRate * err * accordingDFT[j];
                }
            }

            Debug.WriteLine($"Ending learning process of Perceprton for recognition of {Wanted}...");
        }
    }

    
    public static class ListExt
    {
        static Random r = new Random();
        /// <summary> 
        /// Returns a list with 1 promile probability of distortion on each cell.
        /// </summary>
        public static List<int> Distort(this List<int> input)
        {
            int randomNumber;
            var retVal = new List<int>();
            for (int i = 0; i < input.Count; i++)
            {
                randomNumber = r.Next() % 10000;
                if (randomNumber < 1)
                {
                    retVal.Add(-input[i]);
                }
                else
                {
                    retVal.Add(input[i]);
                }
            }
            return retVal;
        }
    }
}
