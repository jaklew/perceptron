﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Perceptrons
{
    public class Result : List<int>
    {
        public override string ToString()
        {
            string retVal = "";
            for(int i = 0; i < Count; i++)
            {
                if (i < (Count - 1))
                {
                    retVal += this[i].ToString() + ",";
                }
                else retVal += this[i];
            }
            return retVal; 
        }

        #region CTOR
        public Result(Result r) : base(r)
        {

        }

        public Result()
        {

        }
        #endregion
    }
}
