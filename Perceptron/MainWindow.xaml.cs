﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

using PropertyChanged;

using static Perceptrons.Constants;

namespace Perceptrons
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    [AddINotifyPropertyChangedInterface]
    public partial class MainWindow : Window
    {
        #region Members
        Dictionary<Canvas, int> ButtonDictionary;
        int[] State = new int[35];
        List<Perceptron> Perceptrons = new List<Perceptron>(10);
        List<Tuple<int, List<int>>> LearningSet = new List<Tuple<int, List<int>>>();
        List<Tuple<int, List<double>>> DFTs = new List<Tuple<int, List<double>>>();

        public Result Result { get; set; }
        int PredictedResult { get; set; }
        #endregion

        #region CTOR
        public MainWindow()
        {
            ButtonDictionary = new Dictionary<Canvas, int>();
            InitializeComponent();
            int i = 0;
            foreach(StackPanel sp in InputStack.Children.Cast<StackPanel>())
            {
                foreach(Canvas c in sp.Children.Cast<Canvas>())
                {
                    ButtonDictionary.Add(c, i);
                    i++;
                }
            }
            for(int k = 0; k < State.Length; k++)
            {
                State[k] = -1;
            }
            Result = new Result();
            LoadTrainingDataFromFile();
            PreparePerceptrons();
        }
        #endregion

        #region Private methods
        private void HandlePressed(object source, MouseEventArgs e)
        {
            Result.Clear();
            ResultText.Text = "";
            Canvas sourceCanvas = (Canvas)source;
            int value;
            if (ButtonDictionary.TryGetValue(sourceCanvas, out value))
            {
                State[value] = -State[value];

                Brush currentBrush = ((Canvas)source).Background;
                if (State[value] > 0)
                {
                    ((Canvas)source).Background = Brushes.Black;
                }
                else
                {
                    ((Canvas)source).Background = Brushes.White;
                }
            }
            foreach(var p in Perceptrons)
            {
                List<double> vs = DFT.DFT2D(State.ToList(), 35);
                double xD = p.Output(DFT.DFT2D(State.ToList(), 35));
                if(xD > 0)
                {
                    Result.Add(p.Wanted);
                }
                int x = 0;
            }
            ResultText.Text = Result.ToString();
        }

        private void HandleExit(object source, EventArgs e)
        {
            Application.Current.Shutdown();
        }

        

        private void PreparePerceptrons()
        {
            for(int i = 0; i < 10; i++)
            {
                Perceptron trainingPerceptron = new Perceptron(i, DFTs);
                trainingPerceptron.Train(LearningSet);
                Perceptrons.Add(trainingPerceptron);
            }
        }

        private void LoadTrainingDataFromFile()
        {
            LearningSet = trainingData;
            foreach(var tup in LearningSet)
            {
                DFTs.Add(new Tuple<int, List<double>>(tup.Item1, DFT.DFT2D(tup.Item2, tup.Item2.Count)));
            }
        }

        private Stack<List<int>> LoadTrainingData(int i)
        {
            return new Stack<List<int>>(LearningSet.Where(v => v.Item1 == i).Select(v => v.Item2));
        }
        #endregion

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            foreach(Canvas c in ButtonDictionary.Keys)
            {
                c.Background = Brushes.White;
            }
            Result.Clear();
            ResultText.Text = "";
        }

        private void Chart_Click(object sender, EventArgs e)
        {
            Window window = new PopupWindow(Perceptrons);
            window.ShowDialog();
        }
    }
}
