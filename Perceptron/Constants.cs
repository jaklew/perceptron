﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Perceptrons
{
    public sealed class Constants
    {
        public const double EPSILON = 1e-8;
        public const int NO_OF_ITERATIONS = 100000;
        public const double LEARNING_RATE = 0.0001;


        public static List<Tuple<int, List<int>>> trainingData = new List<Tuple<int, List<int>>>
        {
            new Tuple<int, List<int>>(0, new List<int>{ -1,-1,-1,-1,-1,-1,1,1,1,-1,-1,1,-1,1,-1,-1,1,-1,1,-1,-1,1,-1,1,-1,-1,1,1,1,-1,-1,-1,-1,-1,-1 }),
            new Tuple<int, List<int>>(1, new List<int>{ -1,-1,-1,-1,-1,-1,-1,1,-1,-1,-1,1,1,-1,-1,1,-1,1,-1,-1,-1,-1,1,-1,-1,-1,-1,1,-1,-1,-1,-1,-1,-1,-1 }),
            new Tuple<int, List<int>>(2, new List<int>{ -1,-1,-1,-1,-1,-1,1,1,1,-1,-1,-1,-1,1,-1,-1,1,1,1,-1,-1,1,-1,-1,-1,-1,1,1,1,-1,-1,-1,-1,-1,-1 }),
            new Tuple<int, List<int>>(3, new List<int>{ -1,-1,-1,-1,-1,-1,1,1,1,-1,-1,-1,-1,1,-1,-1,1,1,1,-1,-1,-1,-1,1,-1,-1,1,1,1,-1,-1,-1,-1,-1,-1 }),
            new Tuple<int, List<int>>(4, new List<int>{ -1,-1,-1,-1,-1,-1,1,-1,1,-1,-1,1,-1,1,-1,-1,1,1,1,-1,-1,-1,-1,1,-1,-1,-1,-1,1,-1,-1,-1,-1,-1,-1 }),
            new Tuple<int, List<int>>(5, new List<int>{ -1,-1,-1,-1,-1,-1,1,1,1,-1,-1,1,-1,-1,-1,-1,1,1,1,-1,-1,-1,-1,1,-1,-1,1,1,1,-1,-1,-1,-1,-1,-1 }),
            new Tuple<int, List<int>>(6, new List<int>{ -1,-1,-1,-1,-1,-1,1,1,1,-1,-1,1,-1,-1,-1,-1,1,1,1,-1,-1,1,-1,1,-1,-1,1,1,1,-1,-1,-1,-1,-1,-1 }),
            new Tuple<int, List<int>>(7, new List<int>{ -1,-1,-1,-1,-1,-1,1,1,1,-1,-1,-1,-1,1,-1,-1,-1,-1,1,-1,-1,-1,-1,1,-1,-1,-1,-1,1,-1,-1,-1,-1,-1,-1 }),
            new Tuple<int, List<int>>(8, new List<int>{ -1,-1,-1,-1,-1,-1,1,1,1,-1,-1,1,-1,1,-1,-1,1,1,1,-1,-1,1,-1,1,-1,-1,1,1,1,-1,-1,-1,-1,-1,-1 }),
            new Tuple<int, List<int>>(9, new List<int>{ -1,-1,-1,-1,-1,-1,1,1,1,-1,-1,1,-1,1,-1,-1,1,1,1,-1,-1,-1,-1,1,-1,-1,1,1,1,-1,-1,-1,-1,-1,-1 }),

        };
    }
}
